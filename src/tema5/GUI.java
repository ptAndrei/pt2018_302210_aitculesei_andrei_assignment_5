package tema5;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class GUI {

	private JFrame fereastra = new JFrame("Tema 5");
	
	public JButton btn1 = new JButton("countDistinctDays");
	public JButton btn2 = new JButton("countNumberActivities");
	public JButton btn3 = new JButton("getTimeDifference");
	public JButton btn4 = new JButton("getActionsPerDay");
	
	public GUI() {
		
		fereastra.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fereastra.setVisible(true);
		fereastra.setLayout(new GridLayout(2,2));
		fereastra.setSize(new Dimension(500, 500));
		
		fereastra.add(btn1);
		fereastra.add(btn2);
		fereastra.add(btn3);
		fereastra.add(btn4);
		
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				long number = MainClass.countDistinctDays(MainClass.MDList);
				JOptionPane.showMessageDialog(null, number);
			}
		});
		
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MainClass.countNumberActivities(MainClass.MDList);
			}
		});
		
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MainClass.getTimeDifference(MainClass.MDList);
			}
		});
		
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				MainClass.getActionsPerDay(MainClass.MDList);
			}
		});
	}
}
