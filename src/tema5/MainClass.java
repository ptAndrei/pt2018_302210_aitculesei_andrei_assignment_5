package tema5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainClass {

	public static List<MonitoredData> MDList; 
	
	public static void main(String[] args) {
		
		List<String> list = new ArrayList<>();
		Stream<String> lines;
		try {
			
			lines = Files.lines(Paths.get("C:/Users/Administrator/eclipse-workspace/tema5/tema5/Activities.txt"));
			list = lines.collect(Collectors.toList());
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		//List<MonitoredData> MDList = new ArrayList<>();
		MDList = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			
			StringTokenizer st = new StringTokenizer(list.get(i));
			
			String start = "";
			String end = "";
			String activity = "";
			int index = 0;
			while(st.hasMoreTokens()) {
				
				if (index == 0) start += st.nextToken();
				else if (index == 1) start += " "+st.nextToken();
				else if (index == 2) end += st.nextToken();
				else if (index == 3) end +=  " "+st.nextToken();
				else
					activity += st.nextToken();
				index++;
			}
			MonitoredData MDdata = new MonitoredData(start, end, activity);
			MDList.add(MDdata);
		}
		new GUI();
	}
	
	public static long countDistinctDays(List<MonitoredData> list) {
		
		long number;
		number = list.stream().map(MDdata -> MDdata.getEnd_time().substring(0,10)).distinct().count();
		return number;
	}
	
	public static void countNumberActivities(List<MonitoredData> list) {
		
		Map<String, Integer> map = list.stream().collect(Collectors.groupingBy(act -> act.getActivity_label(), Collectors.summingInt(sum -> 1)));
		System.out.println(map);
	}
	
	public static long durationDifference(MonitoredData obj) {
		
		String date1 = obj.getStart_time();
		String date2 = obj.getEnd_time();
		Date d1, d2;
		try {
			d1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(date1);
			d2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(date2);

			return (d2.getTime() - d1.getTime()) /(1000*60);
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		return 0;
	}
	
	public static void getTimeDifference(List<MonitoredData> list) {
		
		list.stream().forEach(el -> System.out.println(el.getActivity_label() + " " +durationDifference(el)));
	}
	
	public static void getActionsPerDay(List<MonitoredData> list) {
		
		Map<String, Map<String, Integer>> map = list.stream().collect(Collectors.groupingBy(date -> date.getEnd_time().substring(0, 10), Collectors.groupingBy(activities -> activities.getActivity_label(), Collectors.summingInt(sum -> 1))));
		System.out.println(map);
	}
}
